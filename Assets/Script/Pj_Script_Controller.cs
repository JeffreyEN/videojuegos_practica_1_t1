using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pj_Script_Controller : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;
    public float upSpeed = 60;
    public float RunSpeed = 20;
    public float WalkSpeed = 10;
    bool EstaTocandoElSuelo = false;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }

    void Update()
    {

        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX = false;
            setWalkAnimation();
            rb2d.velocity = new Vector2(WalkSpeed, rb2d.velocity.y);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                setRunAnimation();
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
            }
            if (Input.GetKey(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }

        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setWalkAnimation();
            rb2d.velocity = new Vector2(-WalkSpeed, rb2d.velocity.y);

            if (Input.GetKey(KeyCode.LeftShift))
            {
                setRunAnimation();
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);
            }
            if (Input.GetKey(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }
        }
        else if (Input.GetKey(KeyCode.Space) && EstaTocandoElSuelo)
        {
            setJumpAnimation();
            rb2d.velocity = Vector2.up * upSpeed;
            EstaTocandoElSuelo = false;
        }
        else if (Input.GetKey(KeyCode.Z))
        {
            setAttackAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);

        }
        else
        {
            setIdleAnimation();
        }


    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        EstaTocandoElSuelo = true;
    }
    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setRunAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setAttackAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }
    private void setWalkAnimation()
    {
        _animator.SetInteger("Estado", 4);
    }
}


